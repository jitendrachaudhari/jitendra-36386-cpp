#include"../include/Customer.h"
#include"../include/Vehicle.h"
using namespace kd3;

Customer::Customer():name(),mobile(),address()
{}
	Customer::Customer(char *name, char *mobile, char *address){
		strcpy(this->name,name);
		strcpy(this->address,address);
		strcpy(this->mobile,mobile);
	}

	void Customer::setAddress(char *address){
		strcpy(this->address,address);
	}
	 const char* Customer::getAddress() const {
		return address;
	}

	void Customer::setMobile(char *mobile){
		strcpy(this->mobile,mobile);
	}
	 const char* Customer::getMobile() const {
		return mobile;
	}

	void Customer::setName(char *name){
		strcpy(this->name,name);
	}
	const char*Customer:: getName() const {
		return name;
	}
	void Customer::setVehicle(Vehicle &v){
		this->vehicle.push_back(v);
	}
	vector<Vehicle>& Customer::getVehicleList(){
		return this->vehicle;
	}
	void Customer::displayVehicle(){
		for(size_t index=0;index<this->vehicle.size();index++)
			cout<<index+1<<" "<<this->vehicle.at(index).getCompany()<<" "
			<<this->vehicle.at(index).getModel()<<" "
			<<this->vehicle.at(index).getNumber()<<endl;

	}
	void Customer::loadCustomer(ifstream &fin){
		if(!fin){
			cout<<"File Not Open "<<endl;
			return;
		}
		int ch;
		while((ch=fin.get())!=-1){
			cout<<(char)ch;
		}
	}
	 void Customer::storeCustomer(ofstream& fcustomer){
		 ofstream  fvehicle;
		 unsigned int  j=0;
		 fvehicle.open("vehicle.csv",ios::app);
		 if(!fcustomer) {
			 cout<<"failed to open customer file";
			 return;
		 }
		 if(!fvehicle) {
			 cout<<"failed to open vehicle file";
			 return;
		 }
		 fcustomer << name << "," << address << ","
				 <<mobile  <<  endl;

		 cout<<address <<endl;
		 cout<<mobile<<endl;
		 vector <Vehicle> vehicle=this->getVehicleList();
		 for( j=0;j<vehicle.size(); j++) {
			 fvehicle   << vehicle[j].getCompany() << ","
					 <<vehicle[j].getModel() << ","
					 <<vehicle[j].getNumber()<< ","
					 <<this->mobile << endl;
		 }
		 fvehicle.close();


	}
