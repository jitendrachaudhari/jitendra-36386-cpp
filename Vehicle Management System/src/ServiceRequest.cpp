
#include"../include/Service.h"
#include"../include/ServiceRequest.h"
using namespace std;

using namespace kd3;
ServiceRequest::ServiceRequest(char* custName,char* vehNumber){
		strcpy(this->name,custName);
		strcpy(this->vehNumber,vehNumber);
	}

	const char* ServiceRequest::getName() const {
		return name;
	}
	void ServiceRequest::setName(char *name){
		strcpy(this->name,name);
	}

	const list<Service*>& ServiceRequest::getServList() const {
		return servList;
	}

	const char* ServiceRequest::getVehNumber() const {
		return vehNumber;
	}
	void ServiceRequest::setVehNumber(char* vehNumber){
		strcpy(this->vehNumber,vehNumber);
	}
	void ServiceRequest::addItem(Service *serv){
	   servList.push_back(serv);
	}
	void ServiceRequest::processRequest(){

	}

