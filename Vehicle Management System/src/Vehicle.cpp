
#include"../include/Vehicle.h"
using namespace kd3;
Vehicle::Vehicle():company(),model(),number()
{}
Vehicle::Vehicle(char *company,char *model,char *number){
	strcpy(this->company,company);
	strcpy(this->model,model);
	strcpy(this->number,number);
}


void Vehicle::setCompany(char *company){
	strcpy(this->company,company);
}
const char* Vehicle::getCompany() const {
	return company;
}

void Vehicle::setModel(char *model){
	strcpy(this->model,model);
}
 const char* Vehicle::getModel() const {
	return model;
}

void Vehicle::setNumber(char *number){
	strcpy(this->number,number);
}
const char* Vehicle::getNumber() const {
	return number;
}



