

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
#include<iostream>
#include<cstring>
#include <fstream>
#include<vector>
#include"../include/Vehicle.h"
using namespace std;
namespace kd3{
class Customer{
	char name[50];
	char mobile[10];
	char address[50];
	vector<Vehicle> vehicle;
public:
	Customer();
	Customer(char *name, char *mobile, char *address);
	void setAddress(char *address);
	 const char* getAddress() const ;

	void setMobile(char *mobile);
	 const char* getMobile()const ;

	void setName(char *name);
	const char* getName() const ;
	void setVehicle(Vehicle &v);
	vector<Vehicle>& getVehicleList();
	void displayVehicle();
	static void loadCustomer(ifstream &fin);
    void storeCustomer(ofstream& fcustomer);

	friend istream& operator>>(istream &cin,Customer &other){
		cout<<"Enter Name	:	";
		cin>>other.name;
		cout<<"Enter Address	:	";
		cin>>other.address;
		cout<<"Enter Mobile	No.	:	";
		cin>>other.mobile;
		return cin;
	}
	friend ostream& operator<<(ostream &cout,Customer &other){
		cout<<"Name	:	"<<other.name<<" ";
		cout<<"Address	:	"<<other.address<<" ";
		cout<<"Mobile	:	"<<other.mobile<<endl;
		for(size_t index=0;index<other.vehicle.size();index++)
			cout<<index+1<<" "<<other.vehicle.at(index).getCompany()<<" "
			<<other.vehicle.at(index).getModel()<<" "
			<<other.vehicle.at(index).getNumber()<<endl;
		return cout;
	}

};

}




#endif /* CUSTOMER_H_ */
