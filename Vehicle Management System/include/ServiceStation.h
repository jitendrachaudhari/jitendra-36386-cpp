

#ifndef SERVICESTATION_H_
#define SERVICESTATION_H_
#include<iostream>
#include<cstring>
#include<vector>
#include"../include/Bill.h"
#include "../include/Customer.h"
using namespace std;
namespace kd3{
class ServiceStation{
	vector<Bill>billList;
	vector<Customer>custList;
	char name [50];

	ServiceStation(char *name);
	ServiceStation(const ServiceStation& other){
		strcpy(this->name,other.name);
	}
public:
	vector<Bill>& getBillList() const;
	//const vector<Customer>& getCustList() const ;

	const char* getName() const ;
	double computeCash();
	void printCustomerListRecord();
	void handleServiceRequest();
	void newCustomer();
	void storeCustomerDetails();
	void loadCustomerDetails();
	Customer* findCustomer(char* name);
	static ServiceStation& getInstance(char *name);
};
}




#endif /* SERVICESTATION_H_ */
