

#ifndef BILL_H_
#define BILL_H_

#include<iostream>
#include<cstring>
#include"../include/ServiceRequest.h"
using namespace std;
namespace kd3{
class Bill{
	double amount;
	double paidAmount;
	ServiceRequest* req;
public:
	Bill(ServiceRequest* req);
	double computeAmount();
	double computeTax();

	double getPaidAmount() const ;


	void setPaidAmount(double paidAmount) ;
	void printRecord();
};
}



#endif /* BILL_H_ */
