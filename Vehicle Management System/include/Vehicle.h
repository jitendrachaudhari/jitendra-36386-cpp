

#ifndef VEHICLE_H_
#define VEHICLE_H_
#include<iostream>
#include<cstring>
#include <fstream>
#include<vector>
using namespace std;
namespace kd3{
class Vehicle{
	char company[50];
	char model[10];
	char number[50];
public:
	Vehicle();
	Vehicle(char *company,char *model,char *number);

	friend istream& operator>>(istream &cin,Vehicle &other){
		cout<<"Enter Company	:	";
		cin>>other.company;
		cout<<"Enter Model	:	";
		cin>>other.model;
		cout<<"Enter Number	:	";
		cin>>other.number;

		return cin;
	}

	friend ostream& operator<<(istream &cin,Vehicle &other){
		cout<<"Company	:	"<<other.company<<" ";
		cout<<"Model	:	"<<other.model<<" ";
		cout<<"Number	:	"<<other.number<<endl;
		return cout;
	}
	void setCompany(char *company);
	const char* getCompany() const;

	void setModel(char *model);
	 const char* getModel() const;

	void setNumber(char *number);
	const char* getNumber() const ;
};

}



#endif /* VEHICLE_H_ */
