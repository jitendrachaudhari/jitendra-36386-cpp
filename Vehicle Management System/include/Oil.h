

#ifndef OIL_H_
#define OIL_H_
#include"../include/Service.h"
namespace kd3{
class Oil:public Service{
	double cost;
public:
	Oil();
	Oil(char * desc,double cost);

	double getCost() const ;

	void setCost(double cost) ;
	void printRecord();
	void acceptRecord();
	double price();
};
}




#endif /* OIL_H_ */
