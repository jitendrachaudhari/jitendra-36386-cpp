

#ifndef SERVICE_H_
#define SERVICE_H_
#include<iostream>
#include<cstring>
namespace kd3{
class Service{
	char desc[50];
public:
	Service();
	Service(char *desc);
	const char* getDesc() const ;
	void setDesc(char *desc);
	void Display();
	void acceptRecord();
	double price();
};
}

#endif /* SERVICE_H_ */
