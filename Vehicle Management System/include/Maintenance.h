

#ifndef MAINTENANCE_H_
#define MAINTENANCE_H_
#include "../include/Parts.h"
#include "../include/Service.h"
#include <iostream>
#include <list>
#include <iterator>
using namespace std;
namespace kd3{
class Maintenece:public Service{
	double labourCharges;
     list<Part>partList;
public:
	Maintenece();
	Maintenece(char *desc,double labourCharges);
	double getLabourCharges() const ;

	void setLabourCharges(double labourCharges) ;
	list<Part>& getPartList();
	void addPart(Part& newPart);
	void printRecord();
};
}




#endif /* MAINTENANCE_H_ */
