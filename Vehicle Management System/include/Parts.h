

#ifndef PARTS_H_
#define PARTS_H_
#include<cstring>
#include<iostream>
namespace kd3{
class Part{
	char desc[50];
	double rate;
public:
	Part();
	Part(char *desc,double rate);
	const char* getDesc() const ;
	void setRate(char *desc);

    void acceptRecord();
	double getRate() const ;

	void setRate(double rate) ;
};
}



#endif /* PARTS_H_ */
