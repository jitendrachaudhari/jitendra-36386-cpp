

#ifndef SERVICEREQUEST_H_
#define SERVICEREQUEST_H_

#include <iostream>
#include<cstring>
#include<list>
#include"../include/Service.h"
using namespace std;
namespace kd3{
class ServiceRequest{
	char name [50];
	char vehNumber[50];
	list<Service*>servList;
public:
	ServiceRequest(char* custName,char* vehNumber);
	const char* getName() const;
	void setName(char *name);
	const list<Service*>& getServList() const ;
	const char* getVehNumber() const ;
	void setVehNumber(char* vehNumber);
	void addItem(Service *serv);
	void processRequest();
};
}



#endif /* SERVICEREQUEST_H_ */
