

#include<iostream>
#include<string.h>
#include"../include/IllegalArgumentException.h"
#include"../include/Account.h"
#include"../include/Bank.h"
using namespace std;


void acceptDetails(Account *ptr);

void printDetails(Account *ptr,Bank*ptr1, int number);

int menu_list( void );



int main()
{
	try{
		Account acc;
		Bank bank;
		int number;
		float amount,balance;
		int choice;
		while( ( choice = ::menu_list( ) ) != 0 )
		{
			try{
				switch( choice )
				{
				case 1:
					acceptDetails(&acc);
					number=bank.createAccount(acc);
					cout<<"Account Number	:	"<<number<<endl;
					break;
				case 2:
					number=bank.acceptAccountNo();
					amount=bank.acceptAmount();
					balance=bank.depositAmount(number,amount);
					bank.printBalance(balance);


					break;
				case 3:
					number=bank.acceptAccountNo();
					amount=bank.acceptAmount();
					balance=bank.withdrawAmount(number,amount);
					bank.printBalance(balance);
					break;

				case 4:
					printDetails(&acc,&bank,number);
					break;
				}
			}
			catch(IllegalArgumentException &ex)
			{
				cout<<ex.getMessage()<<endl;
			}
		}
	}
	catch(...)
	{
		cout<<"Exception"<<endl;
	}
	return 0;
}

void acceptDetails(Account *ptr)
{
	string name;
	cout<<"Account  Name	:	";
	cin>>name;
	ptr->setName(name);
	//	int accountNo;
	//	cout<<"Account Number	:	";
	//	cin>>accountNo;
	//	ptr->setAccountNo(accountNo);
	string type;
	cout<<"Account Type	:	";
	cin>>type;
	ptr->setType(type);
	float balance;
	cout<<"Account Balance	:	";
	cin>>balance;
	ptr->setBalance(balance);
}

void printDetails(Account *ptr,Bank*ptr1, int number)
{
	cout<<"Account Name	:	"<<ptr->getName()<<endl;
	cout<<"Account No	:	"<<ptr->getAccountNo()<<endl;
	cout<<"Account Type	:	"<<ptr->getType()<<endl;
	cout<<"Account Balance	:	"<<ptr->getBalance()<<endl;
}

int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create Account"<<endl;
	cout<<"2.Deposit Amount"<<endl;
	cout<<"3.Withdraw Amount"<<endl;
	cout<<"4.Print Details"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}




