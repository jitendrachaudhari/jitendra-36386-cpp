
#include<iostream>
#include"../include/Bank.h"
#include"../include/Account.h"
#include"../include/IllegalArgumentException.h"
using namespace std;
int count=1000;

int Bank:: createAccount(Account acc)
	{
		acc.accountNo=++count;
		arr[++index]=acc;
		return acc.accountNo;
	}
	int Bank::acceptAccountNo()
	{
		int number;
		cout<<"Account No	:	";
		cin>>number;
		if(number<1000)
			throw IllegalArgumentException("Account Number Must be 4 digit number");
		return number;
	}

	float Bank:: acceptAmount()
	{
		float amount;
		cout<<"Amount	:	";
		cin>>amount;
		return amount;
	}

	void Bank:: printBalance(float balance)
	{
		cout<<"Account Balance	:	"<<balance<<endl;
	}

	float Bank:: withdrawAmount(int number, float amount)
	{
		for(int i=0;i<=index;++i)
		{
			if(arr[i].accountNo==number)
			{
				if(arr[i].balance<amount)
					throw IllegalArgumentException("Insufficient Balance");
				arr[i].balance=arr[i].balance-amount;
				return arr[i].balance;
			}

		}
		return 0;
	}

	float Bank:: depositAmount(int number, float amount)
	{
		for(int i=0;i<=index;++i)
		{
			if(arr[i].accountNo==number)
			{
				if(amount<0)
					throw IllegalArgumentException("No negative amount can be deposited");
				arr[i].balance=arr[i].balance+amount;
				return arr[i].balance;
			}

		}
		return 0;
	}


