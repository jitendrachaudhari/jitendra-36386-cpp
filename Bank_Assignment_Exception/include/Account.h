
#ifndef ACCOUNT_H_
#define ACCOUNT_H_
#include<string>
using namespace std;

class Account{
private:
	string name;
	string accountType;

public:
	int accountNo;
	float balance;
	Account( void );
	void setName( string name);
	void setAccountNo( int accountNo);
	void setType(string type);
	void setBalance(float balance);

	string getName( void );

	int getAccountNo( void );
	string getType( void );
	float getBalance( void );

};




#endif /* ACCOUNT_H_ */
