
#ifndef BANK_H_
#define BANK_H_
#include"../include/Account.h"
#include"../include/IllegalArgumentException.h"
class Bank{
public:
	Account arr[5];
	int index=0;
public:
	int createAccount(Account acc);
	int acceptAccountNo();

	float acceptAmount();

	void printBalance(float balance);

	float withdrawAmount(int number, float amount);

	float depositAmount(int number, float amount);

};



#endif /* BANK_H_ */
