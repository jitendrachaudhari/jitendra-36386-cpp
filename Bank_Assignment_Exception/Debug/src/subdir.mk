################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Account.cpp \
../src/Bank.cpp \
../src/IllegalArgumentException.cpp \
../src/main.cpp 

OBJS += \
./src/Account.o \
./src/Bank.o \
./src/IllegalArgumentException.o \
./src/main.o 

CPP_DEPS += \
./src/Account.d \
./src/Bank.d \
./src/IllegalArgumentException.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


