#include<iostream>
#include<cstdlib>
#include"../include/Point.h"
using namespace std;

void accept_position( Point *ptr  )
{
	int x;
	cout<<"X Position 	:	";
	cin>>x;
	ptr->setXPosition(x);
	int y;
	cout<<"Y Position 	:	";
	cin>>y;
	ptr->setYPosition(y);
}

void print_position(Point *ptr)
{
	int result;
	result=ptr->getXPosition();
	cout<<"X Position 	:	"<<result<<endl;
	cout<<"Y Position 	:	"<<ptr->getYPosition()<<endl;
}


int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept "<<endl;
	cout<<"2.Display"<<endl;
	cout<<"3.Modify"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}

Point::Point( void )
{
	this->xPosition=10;
	this->yPosition=20;
}
Point::	Point( const int xPosition, const int yPosition):xPosition(xPosition),yPosition(yPosition)
{

}

void Point::setXPosition( const  int xPosition)
{
	this->xPosition=xPosition;
}

void Point::setYPosition( const int yPosition)
{
	this->yPosition=yPosition;
}

int Point::getXPosition( void )const
{
	return this->xPosition;
}

int Point::getYPosition( void )const
{
	return this->yPosition;
}



