
#ifndef POINT_H_
#define POINT_H_

class Point{
private:
	int xPosition;
	int yPosition;
public:
	Point( void );

	Point( const int xPosition, const int yPosition);

	void setXPosition( const  int xPosition);

	void setYPosition( const int yPosition);

	int getXPosition( void )const;

	int getYPosition( void )const;
};




#endif /* POINT_H_ */
