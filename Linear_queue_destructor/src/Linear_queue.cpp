#include <iostream>
#include<cstring>
#include<stdlib.h>

using namespace std;

class Queue
{
private:
    int value;
    int rear;
    int front;
    int size;
    int *arr_queue;

public:

    Queue( void ):value(0), rear(0), front(0), size(0), arr_queue(NULL)
         {    }
    Queue( int size )
    {
    	cout<<"Inside Constructor"<<endl;
    	this->value = 0;
    	this->front = 0;
    	this->rear = 0;
    	this->size = size;
    	this->arr_queue = new int[size];
    }
    void insert() {
        if (rear == this->size)
            cout << "Queue Reached Max..!"<<endl;
        else {
            cout << "Enter The Value to be Insert : "<<endl;
            cin>>value;
            cout << " Position : " << this->rear + 1 << " , Insert Value  : " << value <<endl;
            this->arr_queue[rear++] = value;
        }
    }

    void removeData() {
        if (front == rear)
            cout << "Queue is Empty..!"<<endl;
        else {
            cout << "Position : " << front << " , Remove Value  :" << this->arr_queue[front]<<endl;
            front++;
        }
    }

    void display() {
        cout << " Queue Size : "  << (rear - front)<<endl;
        for (int i = front; i < rear; i++)
            cout <<" Position : " << i << " , Value  : " << this->arr_queue[i]<<endl;
    }
    ~Queue( void )
             {    cout<<"Inside Destructor"<<endl;
             	 	 this->clear( );
             }
private:
    void clear ( )
    {
    	if (arr_queue != NULL)
    	{
    		delete[ ] this->arr_queue;
    		arr_queue = NULL;
    	}
    }
};
int menu_list( )
{
	int choice;
	cout << "\nO.Exit\n1.Insert \n2.Remove \n3.Display";
	cout << "\nEnter Your Choice : ";
	cin>>choice;
	return choice;
}


int main() {
    int choice;
    //Queue q1;
    Queue *ptr = new Queue(10);

    while( ( choice = menu_list( ) ) != 0)
    {
        switch (choice) {
            case 1:
                ptr->insert();
                break;
            case 2:
            	 ptr->removeData();
                break;
            case 3:
            	 ptr->display();
                break;
            default:
                cout<<"Wrong Choice";
                break;
        }
    };
delete ptr;
ptr = NULL;
    return 0;
}
