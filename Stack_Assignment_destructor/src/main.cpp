#include<iostream>
using namespace std;
#include"../include/stack.h"
int menu_list( )
{
	int choice;
	cout<<endl<<"0.Exit"<<endl<<"1.Push"<<endl<<"2.Pop"<<endl<<"3.Peek"<<endl;
	cout<<"Enter a Choice";
	cin>>choice;
	return choice;
}
int main()
{
	Stack stk;
	int choice,number,result;
	while ( ( choice = menu_list( )) !=0 )
	{
		switch(choice)
		{
		case 1: cout<<"Enter Element to push";
				cin>>number;
				result = stk.push(number);
				if(result==0)
				{
					cout<<"stack is full";
				}
				cout<<result<<" - is Pushed on Stack"<<endl;
				break;
		case 2:
				result=stk.pop();
				if (result==0)
				{
					cout<<"stack is empty"<<endl;
				}
				cout<<result<<" - Element pop out"<<endl;
				break;
		case 3: result=stk.peek();
				if(result==0)
					{
						cout<<"Stack is empty"<<endl;
					}
					cout<<result<<"is Peek value"<<endl;
				break;
		default: cout<<"Invalid Choice"<<endl;

		}
	}
	return 0;
}
