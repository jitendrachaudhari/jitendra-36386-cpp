
#ifndef INCLUDE_STACK_H_
#define INCLUDE_STACK_H_

#define MAX 20
class Stack {
    int top;
    int a[MAX]; // Maximum size of Stack
public:
    int isfull();
    int isempty();
    int push(int x);
    int pop();
    int peek();
};

#endif /* INCLUDE_STACK_H_ */
