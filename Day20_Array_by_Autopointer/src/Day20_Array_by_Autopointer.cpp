#include<iostream>
#include<string>
#include<memory>
using namespace std;

class Exception
{
private:
    string message;
public:
     Exception( string message ="Index out of Bond") : message( message )
    {

    }
    string getMessage( void )
    {
        return this->message;
    }
};


template<class T>
class Array
{
private:
    int size;
    T *arr;
public:

    Array( void ) : size( 0 ), arr( NULL )
    {   }
    Array( int size )throw( bad_alloc )
    {
        this->size = size;
        this->arr = new T[ this->size ];
    }


    void acceptRecord( void )
    {
        for( int index = 0; index < this->size; ++ index )
        {
            cout<<"arr["<<index<<"] = ";
            cin>>this->arr[ index ];
        }
    }

    T& operator[]( int index )
    {
        if( index >=0 && index < size )
            return this->arr[ index ];
        throw Exception("Array index out of bounds exception");
    }

    void printRecord( void )
    {
        for( int index = 0; index < this->size; ++ index )
        {
             cout<<"arr["<<index<<"] = "<<this->arr[ index ]<<endl;
        }
    }

    ~Array( void )
    {
        if( this->arr != NULL )
        {
            delete[] this->arr;
            this->arr = NULL;
        }
    }
};


class AutoPointer
{
private:
    Array<int> *ptr;
public:
    AutoPointer( Array<int> *ptr )
    {
        this->ptr = ptr;
    }
    Array<int>* operator->( )
    {
        return this->ptr;
    }
    ~AutoPointer( void )
    {
        delete this->ptr;
    }
};

int main( void )
{
    try
    {


        AutoPointer obj( new Array<int>( -1 ) );
            obj->acceptRecord( );
            //obj.operator ->()->acceptRecord();
            obj->printRecord( );


//
//      Array<string> *ptr = new Array<string>(5);
//      ptr->acceptRecord( );
//      ptr->printRecord( );
//      delete ptr;
    }

    catch( Exception &ex )
    {
        cout<<ex.getMessage()<<endl;
    }

    catch( bad_alloc &ex)
    {
        cout<<ex.what( );
    }
    catch( ... )
    {
        cout<<"Exception"<<endl;
    }

    return 0;
}
