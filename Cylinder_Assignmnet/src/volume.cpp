#include<iostream>
#include"volume.h"
using namespace std;
void cylinder::set_height(int height)
	{
		this->height = height;
	}
void cylinder::set_radias(int radias)
	{
		this->radias = radias;
	}

double cylinder::get_volume( void )
	{
		return this->volume =3.14 * this->radias * this->radias * this->height;
	}

void calculate_volume(cylinder *ptr)
{
	double height;
	cout<<"Enter a Height"<<endl;
	cin>>height;
	ptr->set_height(height);
	double radias;
	cout<<"Enter a radius"<<endl;
	cin>>radias;
	ptr->set_radias(radias);
}

void print_volume(cylinder *ptr)
{
	double volume = ptr->get_volume();
	cout<<"Volume is - "<<volume<<endl;
}
int menu_list()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Calculate Volume"<<endl;
	cout<<"2.Print Volume"<<endl;
	cin>>choice;
	return choice;
}


