
#ifndef VOLUME_H_
#define VOLUME_H_

#include <iostream>
using namespace std;

class cylinder
{
private:
	double height;
	double radias;
	double volume;
public:
	void set_height(int height);
	void set_radias(int radias);
	double get_volume( void );

};
void calculate_volume(cylinder *ptr);
void print_volume(cylinder *ptr);
int menu_list();

#endif /* VOLUME_H_ */
